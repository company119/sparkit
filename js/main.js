var list = document.querySelectorAll('.list');
var list_details = document.getElementById('list-details');
var margin_saved = list_details.style.margin;
Array.from(list).forEach(function(list) {
    list.addEventListener('click', function(e) {
        /// очищаем активные слева
        var neighbours = document.getElementById(e.target.parentNode.id).children;
        for (let i = 0; i < neighbours.length; i++) {
            document.getElementById(neighbours[i].id).classList.remove("active");
        }
        /// устанавливаем активный список слева
        document.getElementById(e.target.id).classList.add("active");
        /// очищаем активные справа
        var neighbours_details = list_details.children;
        for (let i = 0; i < neighbours_details.length; i++) {
            document.getElementById(neighbours_details[i].id).classList.remove("active");
        }
        ///добавляем один активный справа
        try {
            document.getElementById(e.target.id + "-details").classList.add("active");
            document.getElementById("list-details").style.margin = margin_saved;
            insert();
        } catch (e) {
            document.getElementById("list-details").style.margin = "0px 0px 0px 0px";
        }
    })
});

function insert() {
    /// найдем активный элемент
    var lists = document.getElementById('list').children;

    var list_id_active = "";
    for (let i = 0; i < lists.length; i++) {
        if (lists[i].classList.contains("active")) {
            list_id_active = lists[i].id;
            break;
        }
    }

    /// проверка на разрешение и вставка по месту
    if (window.innerWidth <= 600) {
        var parent = document.getElementById(list_id_active);
        parent.insertBefore(list_details, null);
    } else {
        var parent = document.getElementsByClassName("details-info")[0];
        parent.insertBefore(list_details, null);
    }
}

window.addEventListener('resize', insert, true);

window.onload = function() {
    /// проверка на разрешение и вставка по месту
    if (window.innerWidth <= 600) {
        var parent = document.getElementById("list-1");
        parent.insertBefore(list_details, null);
    } else {
        var parent = document.getElementsByClassName("details-info")[0];
        list_details = document.getElementById('list-details');
        parent.insertBefore(list_details, null);
    }
};